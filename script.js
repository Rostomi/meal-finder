const search = document.getElementById("search"),
submit = document.getElementById("submit"),
mealsEl = document.getElementById("meals"),
random = document.getElementById("random"),
resultHeading = document.getElementById("result-heading"),
single_mealEl = document.getElementById("single-meal");



function searchMeal(e) {
    e.preventDefault();

    // clear single meal div 
    single_mealEl.innerHTML = " ";

    // get search term
    const term = search.value;

    // check for empty
    if (term.trim()) {
        let url = `https://www.themealdb.com/api/json/v1/1/search.php?s=${term}`
        fetch(url)
            .then(res => res.json())
            .then(data => {
                // console.log(data);
                resultHeading.innerHTML = `<h2 class="results-heading"> Search results for '${term}': </h2>`;

                if(data.meals === null){
                    resultHeading.innerHTML = "<p class='results-heading'> Oops... nothing have found :(</p>"
                } else {
                    mealsEl.innerHTML = data.meals.map(meal => 
                        `<div class="meal">
                            <img src="${meal.strMealThumb}" alt="${meal.strMeal}"/>
                            <div class="meal-info" data-mealID="${meal.idMeal}">
                                <h3> ${meal.strMeal} </h3>
                            </div>
                        </div>`
                    ).join('')
                }
        })

        // clear search text
        search.value = "";
    } else{
        alert("Enter Some Text")
    }
}

// fetch meal by meal id
function getMealById(mealID){
    const mealOfUrlById = `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${mealID}`
    fetch(mealOfUrlById)
        .then(res => res.json())
        .then(data => {
            const meal = data.meals[0];

            addMealToDOM(meal);
        })
}

// add meal to dom
function addMealToDOM(meal){
    const ingredients = [];

    for (let i = 1; i <= 20; i++) {
        if (meal[`strIngredient${i}`]) {
            ingredients.push(`
                ${meal[
                    `strIngredient${i}`]} - ${meal[`strMeasure${i}`
                ]} `
            )
        } else {
            break;
        }
    }

    single_mealEl.innerHTML = `
        <div class="single-meal">
            <h4>
                ${meal.strMeal}
            </h4>
            <img src="${meal.strMealThumb}" alt="${meal.strMeal}" />
            <div class="single-meal-info">
                ${meal.strCategory ? `<p>${meal.strCategory}</p>` : ''}
                ${meal.strArea ? `<p>${meal.strArea}</p>` : ''}
            </div>
            <div class="main">
                <p>
                    ${meal.strInstructions}
                </p>
                <h5>Ingredients</h5>
                <ul>
                    ${ingredients.map(ing =>`<li>${ing}</li>`).join('')}
                </ul>
            </div>
        </dv>
    `
}

function getRandomMeal(){
    resultHeading.innerHTML = "Your Random Meal:"
    mealsEl.innerHTML = '';

    fetch("https://www.themealdb.com/api/json/v1/1/random.php")
        .then(res => res.json())
        .then(data => {
            const mealForDom = data.meals[0];

            addMealToDOM(mealForDom);
        })
}


// event listeners
submit.addEventListener("submit", searchMeal);
random.addEventListener("click", getRandomMeal)
mealsEl.addEventListener("click", e => {
    const mealInfo = e.path.find(item => {
        if(item.classList) {
            return item.classList.contains("meal-info")
        } else {
            return false
        }
    })
    
    if (mealInfo) {
        const mealID = mealInfo.getAttribute('data-mealID');
        getMealById(mealID);
    }
})